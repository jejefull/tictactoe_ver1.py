

def get_verif(long_str,lag_str):
    '''
    funtion verif if user enter Only digit <=2 o >=0
    :param long_str: x axe
    :param lag_str: y axe
    :return false if not ok
    '''
    if long_str.isdigit() == False:
        return False
    if lag_str.isdigit() == False:
        return False
    long=int(long_str)
    lag=int(lag_str)
    if(long>2 or long<0 or lag>2 or lag<0):
        return False
    else :
        return True

def get_verification_win_in_Longitude(mat,arg):
    '''
    funtion verif if there is winner on one of 3 x axes
    :param mat: matrix[2][2]
    :param arg: whitch player x or o
    :return x if x wins or o if o wins
    '''

    count_x = 0
    count_0 = 0
    # verif if there is winner in x axe
    z = ''
    for z in mat:
        for arg in z:
            if (arg == 'x'):
                count_x = count_x + 1
            if count_x == 3:
                return 'x'
            if (arg == 'o'):
                count_0 = count_0 + 1
            if (count_0 == 3):
                return '0'
        count_x = 0
        count_0 = 0

def get_verification_win_in_Lagitude(mat,arg):
    '''
    funtion verif if there is winner on one of 3 y axes
    :param mat: matrix[2][2]
    :param arg: whitch player x or o
    :return x if x wins or o if o wins
    '''

    count_x=[0,0,0]
    count_0=[0,0,0]

    for n in range(0, 3, 1):
        for m in range(0,3,1):

            if mat[m][n]=='x':
                count_x[n]=count_x[n]+1
            if count_x[n]==3:
                return 'x'
            if mat[m][n]=='o':
                count_0[n]=count_0[n]+1
            if count_0[n]==3:
                return '0'

def get_verification_win_in_left_diag(mat,arg):
    '''
    funtion verif if there is winner on the left diagonal
    :param mat: matrix[2][2]
    :param arg: whitch player x or o
    :return x if x wins or o if o wins
    '''

    count_x=0
    count_0=0
    for n in range(0, 3, 1):
        if(mat[n][n]=='x'):
            count_x=count_x+1
        if(mat[n][n]=='o'):
            count_0=count_0+1
    if(count_x==3):
        return 'x'
    if(count_0==3):
        return 0

def get_verification_win_in_right_diag(mat,arg):
    '''
    funtion verif if there is winner on the right diagonal
    :param mat: matrix[2][2]
    :param arg: whitch player x or o
    :return x if x wins or o if o wins
    '''

    count_x=0
    count_0=0
    m=2
    for n in range (0,3,1):
        if(mat[m][n]=='x'):
            count_x=count_x+1
        if(mat[m][n]=='o'):
            count_0=count_0+1
        m=m-1

    if(count_x==3):
        return 'x'
    if(count_0==3):
        return '0'

def get_verification_if_no_win(mat,arg):
    '''
    funtion verif if there is no winner
    :param mat: matrix[2][2]
    :param arg: whitch player x or o
    :return False if there is no winer
    '''

    z=''
    y=''
    count=0
    for z in mat:
        for y in z:
            if '_' in y:
                count=count+1
    if count==0:
        return False



def get_verif_win(mat,arg):
    '''
    funtion send in all function witch verif if there is winner
    :param mat: matrix[2][2]
    :param arg: whitch player x or o
    :return x if x wins or o if o wins o False if no winner
    '''

    res_win=''

    res_win=get_verification_win_in_Longitude(mat,arg)
    if res_win =='0' or res_win=='x':
        return res_win

    res_win=get_verification_win_in_Lagitude(mat,arg)
    if res_win=='0' or res_win=='x':
        return res_win

    res_win=get_verification_win_in_left_diag(mat,arg)
    if res_win=='0' or res_win=='x':
        return res_win

    res_win=get_verification_win_in_right_diag(mat,arg)
    if res_win=='0' or res_win=='x':
        return res_win

    res_win=get_verification_if_no_win(mat,arg)
    if res_win==False:
        return False


def get_PlayGame(mat,arg):
    '''
    funtion receive input from user, verif if legal input and empty and to function that check if ther is winner
    :param mat: matrix[2][2]
    :param arg: whitch player x or o
    :return x if x wins or o if o wins
    '''
    while True:
        if(arg=='x'):
             x = input("Input longitude: ")
             y = input("Input lagitude: ")
        elif (arg=='o'):
             x = str(random.randint(0, 3))
             y = str(random.randint(0, 3))
             res=False
        if get_verif(x,y)==False:
            print("wrong numbers or letters- inset number 0-2")
        elif get_verifEmpty(int(x),int(y),mat)==False:
            print("case not empty")
        else:
            mat[int(x)][int(y)]=arg
            break

    for n in range (0,3,1):
        print(mat[n])
    return(get_verif_win(mat,arg))


def get_verifEmpty(long,lag,mat):
    '''
    funtion verif if there is  an empty  case
    :param long: x axe
    :param lag: y axe
    :param mat: matrix[2][2]

    :return true if is empty and false if there is 'x' xor 'o'
    '''
    if(mat[long][lag])=='_':
        return True
    else:
        return False

def get_if_x_win(mat,res):
    '''
    funtion from main send true if x wins
    :param mat: matrix[2][2]
    :param res: whitch player x or o
    :return true if x wins
    '''
    if res == 'x':
        print("x Wins")
        return True
    elif res == False:
        print("there is not winer - try an other game")
        mat = [["_", "_", "_"], ["_", "_", "_"], ["_", "_", "_"]]
        get_PlayGame(mat, 'x')

def get_if_o_win(mat,res):
    '''
    funtion from main send true if o wins
    :param mat: matrix[2][2]
    :param res: whitch player x or o
    :return true if o wins
    '''
    if res == '0':
        print("0 wins")
        return True
    elif res == False:
        print("there is not winer - try an other game")
        mat = [["_", "_", "_"], ["_", "_", "_"], ["_", "_", "_"]]
        get_PlayGame(mat, 'x')


def main():
    '''
    funtion main: if there is winner it is out from fonction
    '''
    mat=[["_","_","_"],["_","_","_"], ["_","_","_"]]
    for n in range(0,3,1):
       print(mat[n])
    while True:
        print("x play now")
        res=get_PlayGame(mat,'x')
        win=get_if_x_win(mat,res)
        if win==True:
            break

        print("o play now")
        res=get_PlayGame(mat,'o')
        win=get_if_o_win(mat,res)
        if win==True:
            break


import random

main()